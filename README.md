# GO App load balanced in MiniKube 

## Assumptions
You have Minikube installed with preferred Driver of choice. The driver used for this particular project was Docker. <br />
You have Docker installed <br />

## Building app

We are going to build a GO hello world app and package it into a docker image

The build is done with docker multi-stage builds to keep the final image size as small as possible



in the root of the code folder execute the following

```
❯ docker build -t go_http_hello .
```

## Load app into Minikube

Next we need to load the image we built into minikube

```
❯ minikube image load go_http_hello
```

## Deploy App into Minikube

Now we are going to deloy the app, and create a service that is going to load balance between the replicas.

```
❯ kubectl apply -f go_http_hello.yaml
```

you should now see the service available either using the kubectl command or the minikube command

```
❯ kubectl get svc
NAME         TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
hello        LoadBalancer   10.104.43.29   <pending>     8090:32731/TCP   7m29s
kubernetes   ClusterIP      10.96.0.1      <none>        443/TCP          9m50s
```

```
❯ minikube service list
|-------------|------------|--------------|-----|
|  NAMESPACE  |    NAME    | TARGET PORT  | URL |
|-------------|------------|--------------|-----|
| default     | hello      |         8090 |     |
| default     | kubernetes | No node port |
| kube-system | kube-dns   | No node port |
|-------------|------------|--------------|-----|

```

## Access the App

to be able to access the service execute the following

```
❯ minikube service hello --url
http://127.0.0.1:60325
❗  Because you are using a Docker driver on darwin, the terminal needs to be open to run it.
```
In the code block above, it says the temrinal needs to be kept open for me to be able to access the service. This could differ depending on the platform you are using. Also, the URL will be different for you as well. <br >
Now in another terminal, send several curl commands to the URL to see the responses

```
❯ curl http://127.0.0.1:60325
Hello, world! You have called me 1 times.
❯ curl http://127.0.0.1:60325
Hello, world! You have called me 2 times.
❯ curl http://127.0.0.1:60325
Hello, world! You have called me 1 times.
❯ curl http://127.0.0.1:60325
Hello, world! You have called me 3 times.
❯ curl http://127.0.0.1:60325
Hello, world! You have called me 2 times.
```
we can see that the count is not going up consecutively, indicating it is being load balanced between the pods.

to close the minikube service just do a command+c or ctl+c