package main

import (
	"fmt"
	"log"
	"net/http"
)

var calls = 0

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		calls++
		fmt.Fprintf(w, "Hello, world! You have called me %d times.\n", calls)
	})
	fmt.Printf("Server running (port=8090), route: http://localhost:8090/\n")
	if err := http.ListenAndServe(":8090", nil); err != nil {
		log.Fatal(err)
	}
}
